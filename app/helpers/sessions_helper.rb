module SessionsHelper
  def sign_in(user)
    user_id = user.id
    cookies.permanent[:user_id] = user_id
    self.current_user = user
  end

  def signed_in?
    !current_user.nil?
  end

  def current_user=(user)
    @current_user = user
  end

  def current_user
    @current_user ||= User.find(cookies[:user_id]) unless cookies[:user_id].nil?
  end

  def current_user?(user)
    user == current_user
  end

  def signed_in_user
    unless signed_in? || is_admin?
      redirect_to signin_url, warning: "Please sign in."
    end
  end

  def sign_out
    cookies.delete(:user_id)
    self.current_user = nil
  end

  def is_admin?
    current_user.role == "admin" if signed_in?
  end

  def is_admin_user
    unless is_admin?
      redirect_to root_path, warning: "You are not admin."
    end
  end
end
