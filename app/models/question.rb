class Question < ActiveRecord::Base
  attr_accessor :author, :labels
  has_and_belongs_to_many :answers
  has_and_belongs_to_many :labels
  belongs_to :user

  before_destroy :destroy_answer
  before_create :assign_to_user

  validates :title, presence: true
  validates :message, presence: true

  def assign_to_user
  	self.user = User.find(self.author) 
  end

  def destroy_answer
  	self.answers.destroy_all
  end
end
