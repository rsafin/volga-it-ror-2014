class Answer < ActiveRecord::Base
  attr_accessor :author, :question_id
  has_and_belongs_to_many :questions
  belongs_to :user

   validates :message, presence: true

  before_create do 
  	assign_to_user
  	assign_to_question
  end

  def assign_to_user
  	self.user = User.find(self.author)
  end

  def assign_to_question
  	self.questions << Question.find(self.question_id)
  end
end
