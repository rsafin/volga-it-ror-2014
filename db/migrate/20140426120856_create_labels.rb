class CreateLabels < ActiveRecord::Migration
  def change
    create_table :labels do |t|
      t.string :title
      t.timestamps
    end

    create_table :labels_questions do |t|
      t.belongs_to :label
      t.belongs_to :question
    end
  end
end
