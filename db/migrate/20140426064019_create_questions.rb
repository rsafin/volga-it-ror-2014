class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.string :title
      t.string :message
      t.string :status, default: "open"
      t.belongs_to :user
      t.timestamps
    end

    create_table :answers_questions do |t|
      t.belongs_to :question
      t.belongs_to :answer
    end
  end
end
